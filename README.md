# [Feit Electric Smart Wi-Fi Dimmer](https://www.feit.com/product/smart-wi-fi-dimmer/)

 <img src="images/DIM_WIFI_pack.png" width="200">

> **_Costco 3 pack $49.99_**
>
> [Item 1382685](https://www.costco.com/feit-electric-smart-dimmer%2c-3-pack.product.100518151.html)

# Holtek A/D Flash MCU with EEPROM

[Holtek HT66F0185](https://www.holtek.com/productdetail/-/vg/66F0175_185)

[TuyAPI](https://github.com/codetheweb/tuyapi) A library for communicating with devices that use the Tuya cloud network.

# Reset Instructions

[Factory Reset](docs/reset-instructions.pdf)

# Reverse Engineering

## Man In The Middle

[How to man-in-the-middle proxy your IOT devices](https://robertheaton.com/2019/11/21/how-to-man-in-the-middle-your-iot-devices/)

## TLS Decrypting

[Recording and Decrypting SSL Encrypted Traffic](https://blog.kchung.co/recording-and-decrypting-ssl-encrypted-traffic/)

[RaspAP WiFi access point](https://github.com/billz/raspap-webgui)

## OpenWrt

### Installing Tools on OpenWRT

[LEDE/OpenWRT — TCPDump to Wireshark](https://medium.com/openwrt-iot/lede-openwrt-tcpdump-to-wireshark-4ede3e020cbb)

---

### Running Captures

Packet capture from Android to FEIT dimmer using tcpdump and Wireshark.

root@192.168.3.1 is your OpenWRT router
host 192.168.3.192 is your FEIT dimmer on your LAN

`ssh root@192.168.3.1 "tcpdump -i br-lan -U -s0 -w - host 192.168.3.192" | /Applications/Wireshark.app/Contents/MacOS/Wireshark -k -i -`

> **_Network Protocol Captures_**

> **_Wireshark MacOS_**
> ![](images/android-feit-04-bootup-2020-04-03_pcapng.png)

[Android to FEIT](docs/android-feit-01-2020-04-03.pcapng)

[Android to FEIT on/off with app](docs/android-feit-02-on-off-2020-04-03)

[Android to FEIT on/off with app](docs/android-feit-03-on-off-2020-04-03)

[Cold boot FEIT](docs/android-feit-04-bootup-2020-04-03)

> **_Capture Analysis_**
- `ESP_B497C1.lan` hostname of FEIT dimmer on LAN
- `ec2-52-43-232-245.us-west-2.compute.amazonaws.com` AWS connection from FEIT dimmer ESP interface

---

# Alternate Firmware

[Tasmota firmware for ESP8266](https://github.com/arendst/Tasmota)

[Tasmota Google cached wiki for TYWE2S](<https://webcache.googleusercontent.com/search?q=cache:07PuJAn0FG0J:https://github.com/arendst/Tasmota/wiki/CE-Smart-Home---LA-WF3-Wifi-Plug-(TYWE2S)+&cd=1&hl=en&ct=clnk&gl=us&client=firefox-b-1-d>) - [Tasmota cached wiki for TYWE2S](docs/TYWE2S-tasmota-cached.pdf)

# User Guides

[TYWE2S ESP8285 Wi-Fi](https://fccid.io/2ANDL-TYWE2S/User-Manual/Users-Manual-3596121.pdf) - [Cached version](docs/Users-Manual-3596121.pdf)

### Signal Identification

[Changing the firmware on Hyleton 313 Smart Plug](https://znanev.github.io/Hyleton-313-Smart-Plug/)

### Component Identification

> **_TYWE2S WiFi Module_**
> ![](images/img_5323.jpg)

> **_TYWE2S WiFi Module_**
> ![](images/img_5329.jpg)

> **_Top of FEIT PCB_**
> ![](images/img_5324.jpg)

> **_Bottom of FEIT PCB_**
> ![](images/img_5325.jpg)

> **_Holtek A/D Flash MCU with EEPROM_**
> ![](images/img_5326.jpg)

> **_3.3v, GND, RX, TX with TTL-232R-3V3 Cable_**
> ![](images/img_5328.jpg)
